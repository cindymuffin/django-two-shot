from django.urls import path
from receipts.views import (
    AccountListView,
    AccountCreateView,
    CategoriesListView,
    CategoriesCreateView,
    ReceiptListView,
    ReceiptCreateView,
)


urlpatterns = [
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "acounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
    path("categories/", CategoriesListView.as_view(), name="expense_list"),
    path(
        "categories/create/",
        CategoriesCreateView.as_view(),
        name="expense_create",
    ),
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
]
